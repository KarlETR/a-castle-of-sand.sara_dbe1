# A Castle Of Sand

This is a mission where you play as member of FSB Spetsnaz Group Vympel.

Your team is tasked to infiltrate Sahrani and prepare to exfil a High Value Target out of the country.

## Mods Required

All of CUP + ACE + ACEX + Arma 3 Female

Here is a [collection](https://steamcommunity.com/sharedfiles/filedetails/?id=1936849727) with all the required mods.


## Usage

```python
import foobar

foobar.pluralize('word') # returns 'words'
foobar.pluralize('goose') # returns 'geese'
foobar.singularize('phenomena') # returns 'phenomenon'
```

## Quick SQF tests

Go to the famous [SQF virtual machine](https://sqfvm.arma3.io/vm/)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)