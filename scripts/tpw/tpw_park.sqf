/*
TPW PARK - Parked cars near habitable buildings
Version: 1.30
Author: tpw
Date: 20190928
Requires: CBA A3, tpw_core.sqf
Compatibility: SP, MP client
	
Disclaimer: Feel free to use and modify this code, on the proviso that you post back changes and improvements so that everyone can benefit from them, and acknowledge the original author (tpw) in any derivative works. 

To use: 
1 - Save this script into your mission directory as eg tpw_park.sqf
2 - Call it with 0 = [25,300,150,20,10,["str1","str2"],1] execvm "tpw_park.sqf"; where 25 = percentage of houses to spawn cars near (0 = no cars, >50 inadvisable), 300 = radius (m) around player to scan for houses to spawn cars, 150 = radius (m) around player beyond which cars are hidden, 20 = player must be closer than this (m) to a car for it to have its simulation enabled, 10 = maximum cars to spawn regardless of settings, "str1" string of mod car classnames to include, 1 = car alarms will be set off by gunfire or explosions 

THIS SCRIPT WON'T RUN ON DEDICATED SERVERS
*/

if (isDedicated) exitWith {};
if (count _this < 5) exitwith {player sidechat "TPW PARK incorrect/no config, exiting."};
if (_this select 0 == 0) exitwith {};
WaitUntil {!isNull FindDisplay 46};

// READ IN VARIABLES
tpw_park_version = "1.30"; // version string
tpw_park_perc = _this select 0; // percentage of houses with parked cars.
tpw_park_createdist = _this select 1; // cars created within this distance, completely removed past it.
tpw_park_simdist = _this select 2; // cars closer than this have simulation enabled.
tpw_park_include = _this select 3; // strings of mod car classes to include
tpw_park_alarm = _this select 4; // car alarms will be set off if car hit by bullets or explosions

tpw_park_active = true; // global enable/disable
tpw_park_cararray = []; // Array of parked cars
tpw_park_houses = []; // array of houses to park cars near

tpw_park_civcarlist = [
"C_Hatchback_01_F",
"C_Hatchback_01_rallye_F",
"C_Hatchback_01_sportF",
"C_Offroad_01_F",
"C_Offroad_01_repair_F",
"C_Offroad_01_sport_F",
"C_Offroad_01_covered_F",
"C_SUV_01_F",
"C_SUV_01_sport_F",
"C_Van_01_box_F",
"C_Van_01_transport_F",
"C_Van_01_fuel_F"
];

// ADD TANOAN CARS IF AVAILABLE
if (isclass (configfile/"CfgWeapons"/"u_c_man_casual_1_f")) then 
	{
	tpw_park_civcarlist = tpw_park_civcarlist + ["C_Offroad_02_unarmed_F"];
	};
	
// ADD LIVONIAN CARS IF AVAILABLE
if (isclass (configfile/"CfgVehicles"/"c_tractor_01_f")) then 
	{
	tpw_park_civcarlist = tpw_park_civcarlist + ["c_tractor_01_f"];
	};	

// ADD IDAP CARS IF AVAILABLE
if (isclass (configfile/"CfgWeapons"/"U_C_IDAP_Man_cargo_F")) then 
	{
	private ["_cfg"];
	_cfg = (configFile >> "CfgVehicles");
	for "_i" from 0 to ((count _cfg) -1) do 
		{
		if (isClass ((_cfg select _i) ) ) then 
			{
			_cfgName = configName (_cfg select _i);
			if ( (_cfgName isKindOf "car") && {getNumber ((_cfg select _i) >> "scope") == 2} && {["IDAP",str _cfgname] call BIS_fnc_inString}&& {!(["UGV",str _cfgname] call BIS_fnc_inString)}) then 
				{
				tpw_park_civcarlist pushback _cfgname;
				};
			};
		};
	};

// ADD CARS FROM INCLUDE LIST
	{
	private ["_cfg"];
	_cfg = (configFile >> "CfgVehicles");
	for "_i" from 0 to ((count _cfg) -1) do 
		{
		if (isClass ((_cfg select _i) ) ) then 
			{
			_cfgName = configName (_cfg select _i);
			_side = getNumber ((_cfg select _i) >> "side");
			if ( (_cfgName isKindOf "car") && {getNumber ((_cfg select _i) >> "scope") == 2} && {[_x,str _cfgname] call BIS_fnc_inString} && _side == 3) then 
				{
				tpw_park_civcarlist pushback _cfgname;
				};
			};
		};
	} count tpw_park_include;
	
// User blacklist
for "_i" from 0 to (count tpw_park_civcarlist - 1) do	
	{	
	_car = tpw_park_civcarlist select _i;
		{
		if ([_x,str _car] call BIS_fnc_inString) then
			{
			tpw_park_civcarlist set [_i, -1];
			};
		} foreach tpw_core_blacklist;
	};
tpw_park_civcarlist = tpw_park_civcarlist - [-1];		

// SET OFF ALARM IF CAR HIT OR DAMAGED
tpw_park_fnc_hitproc = 
	{
	private ["_car","_sound","_sel"];
	_car = _this select 0;	
	if (_car getvariable ["alarmon",0] == 1) exitwith {};
	if (speed _car > 0 || player in _car || damage _car > 0.5 || tpw_park_alarm == 0) exitwith {};
	_car setvariable ["alarmon",1];
	_sel = _car getvariable "alarmsound";
	_sound = format ["%1%2.ogg",'TPW_SOUNDS\sounds\alarm\alarm',_sel];	
	playsound3d [_sound,_car,false,getposasl _car,3,1,200];
	sleep 30;
	_car setvariable ["alarmon",0];
	};
	
// SCAN HOUSES AND ASSIGN PARKING SPOTS
tpw_park_fnc_assignhouses = 
	{
	private ["_house","_nearroads","_road","_connected","_conroad1","_parkdir","_housedir","_parkpos","_posx","_posy","_car"];
	
	// Scan for habitable houses 
	tpw_park_houses = [tpw_park_createdist] call tpw_core_fnc_screenhouses;
	
	// Assign park status
	for "_i" from 0 to (count tpw_park_houses - 1) do
		{
		_house = tpw_park_houses select _i;
		if (_house getvariable ["tpw_park_assigned",-1] == -1) then // if house has not been assigned
			{
			_house setvariable ["tpw_park_assigned",0]; // assigned no parking, will be ignored in future scans
			
			// Only bother if random percentage OK
			if (random 100 > tpw_park_perc) exitwith {}; 
			
			// Only bother if there is a road near the house
			_nearroads = _house nearRoads 30;	
			if (count _nearroads == 0) exitwith {};						
			_road = _nearroads select 0;
					
			// Only bother if no cars or people nearby
			if (count (_road nearentities [["car","man"],20]) > 0) exitwith {}; 

			// Only bother if road is connected to other road segments
			_connected = roadsconnectedto _road;
			if (count _connected < 2) exitwith {};
			
			// Calculate spawn position at side of road 
			_conroad = _connected select 0;
			_roaddir = [_road, _conroad] call BIS_fnc_DirTo; // direction of road segment
			_parkdir = _roaddir + (180 * round (random 1)); // random direction for car, parallel to road
			_housedir = _roaddir + 90; // direction of house
			_roadpos = getposasl _road;
			_posx = (_roadpos select 0) + (3.75 * sin _housedir);
			_posy = (_roadpos select 1) +  (3.75 * cos _housedir);
			_parkpos = [_posx,_posy,0];
			_car = tpw_park_civcarlist  select (floor (random (count tpw_park_civcarlist )));

			// Assign status to house		
			_house setvariable ["tpw_park_assigned",1]; // assigned parking spot, no car yet
			_house setvariable ["tpw_park_pos",_parkpos]; // parking spot
			_house setvariable ["tpw_park_dir",_parkdir]; // parking direction
			};
			
		// Create parked car 
		if (_house getvariable "tpw_park_assigned" == 1) then
			{
			[_house] spawn tpw_park_fnc_movecars;
			};
		};	
	};									
	
// PRESPAWN ARRAY OF CARS TO MOVE INTO PARKED SPOTS AS NEEDED
tpw_park_fnc_spawncars = 
	{
	tpw_park_sparecars = [];
	tpw_park_carnums = [];
	private ["_spawncar","_car","_ct"];
	_ct = 0;
	for "_i" from 0 to 50 do
		{
		
		_car = tpw_park_civcarlist  select (floor (random (count tpw_park_civcarlist )));
		_spawncar = _car createVehiclelocal [0,0,0];
		_spawncar enablesimulation false;
		_spawncar hideobject true;
		_spawncar allowdamage false;		
		_spawncar setfuel random 0.5;
		_spawncar setdamage 0;	
			
		// Alarm eventhandlers
		_spawncar setvariable ["alarmon",0];
		_sel = floor random 6; // random car alarm
		_spawncar setvariable ["alarmsound",_sel];
		if !(["bicycle",typeof _spawncar] call bis_fnc_instring) then	{_spawncar addeventhandler ["hit",{_this spawn tpw_park_fnc_hitproc}]};
		_spawncar addeventhandler ["explosion",{_this spawn tpw_park_fnc_hitprsetdamageoc}];

		if (alive _spawncar) then
			{
			tpw_park_sparecars pushback _spawncar;
			tpw_park_carnums pushback _ct;
			_ct = _ct + 1;
			} else
			{
			deletevehicle _spawncar;
			};
		};	
	};		
	
// MOVE CARS INTO PARKING SPOTS
tpw_park_fnc_movecars = 
	{
	private ["_house","_pos","_carnum","_spawncar","_dir","_test","_rnd"];	
	sleep random 2;	
	_house = _this select 0;	
	_house setvariable ["tpw_park_assigned",2]; //will not spawn car again (even if car mis-spawned and was deleted)
	_pos = _house getvariable "tpw_park_pos"; 
	_dir = _house getvariable "tpw_park_dir";
	_rnd = floor random count tpw_park_carnums;
	_carnum = tpw_park_carnums deleteat _rnd;
	tpw_park_cararray pushback _carnum;
	_spawncar = tpw_park_sparecars select _carnum;
	
	// Car can't die as a result of misspawning 
	_spawncar addeventhandler ["Dammaged",
		{
		private _car = _this select 0;
		_car setdamage 0;
		_car hideobject true;
		_car enablesimulation false;
		_car setpos [0,0,0];	
		_car removeEventHandler ["Dammaged",0];
		_car setvariable ["tpw_park_hit",1];		
		}];
	
	// Remove car if it hits something	
	_spawncar addeventhandler ["EpeContactStart",
		{
		private _car = _this select 0;
		_car setdamage 0;
		_car hideobject true;
		_car enablesimulation false;
		_car setpos [0,0,0];	
		_car setvariable ["tpw_park_hit",1];
		_car removeEventHandler ["epecontactstart", 0]
		}]; // remove vehicle if it's colliding with something	
	_spawncar hideobject false;
	_spawncar setdamage 0;
	_spawncar enablesimulation true;
	_spawncar setdir _dir;
	_spawncar setposatl _pos;

	//	Allow car to settle										
	[_spawncar,_house] spawn 
		{
		private _car = _this select 0;
		private _house = _this select 1;
		sleep 3;
		_car enablesimulation false; 
		_car allowdamage true;
		_car removeEventHandler ["Dammaged",0];
		_car removeEventHandler ["epecontactstart",0];
		_car setvariable ["tpw_park_house",_house];
		};
	};						
	
// ADD REMOVE PARKED CARS AS APPROPRIATE
tpw_park_fnc_mainloop = 
	{
	while {true} do
		{
		if (tpw_park_active && {speed player < 30}) then
			{
			// Assign houses and spawn parked cars
			0= [] call tpw_park_fnc_assignhouses; 
		
			// Enable/disable simulation, remove distant 
			for "_i" from 0 to (count tpw_park_cararray - 1) do
				{
				private _carnum = tpw_park_cararray select _i;
				private _car = tpw_park_sparecars select _carnum;
						
				// Enable simulation only when player nearby
				if (player distance _car < tpw_park_simdist || damage _car > 0.2) then
					{
					_car enablesimulation true; 
					}
				else
					{
					_car enablesimulation false; 
					};

				// Put distant car back into spare car array and unassign its house	
				if (_car distance player > tpw_park_createdist) then 
					{
					_house = _car getvariable "tpw_park_house";
					// If car is not null, and hasn't been damaged during spawn
					if (!isnil "_house" && {!isnull _house} &&{_car getvariable ["tpw_park_hit",0] == 0}) then 
						{
						_house setvariable ["tpw_park_assigned",1]; // house can spawn its car in future
						};
					_car setvariable ["tpw_park_hit",0];
					_car enablesimulation false;
					_car allowdamage false;
					_car hideobject true;
					_car setposasl [0,0,0];
					tpw_park_carnums pushback _carnum;
					tpw_park_cararray set [_i,-1];
					};
				};
			tpw_park_cararray = tpw_park_cararray - [-1];
			
			// Spare car maintenance 
			if (random 10 > 8) then 
				{
				for "_i" from 0 to (count tpw_park_sparecars - 1) do
					{
					private _car = tpw_park_sparecars select _i;
					// Replace null cars
					if (isnull _car) then
						{
						private _type = tpw_park_civcarlist  select (floor (random (count tpw_park_civcarlist )));
						_car = _type createVehicle [0,0,0];
						tpw_park_sparecars set [_i,_car];	
						};
					// Fix damaged
					if (!(_i in tpw_park_cararray) && {damage _car > 0.05}) then
						{
						_car setdamage 0;
						};
					};	
				};
			};
		sleep 5.33;	
		};	
	};	

// RUN IT	
sleep 10;
0= [] spawn tpw_park_fnc_spawncars;
sleep 5;
0 = [] spawn tpw_park_fnc_mainloop;

while {true} do
	{
	// dummy loop so script doesn't terminate
	sleep 10;
	};