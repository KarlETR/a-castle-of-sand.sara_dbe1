/* 
TPW CIVS - Ambient civilians
Author: tpw 
Date: 20190703
Version: 1.65
Requires: CBA A3, tpw_core.sqf
Compatibility: SP, MP client

Disclaimer: Feel free to use and modify this code, on the proviso that you post back changes and improvements so that everyone can benefit from them, and acknowledge the original author (tpw) in any derivative works.     

To use: 
1 - Save this script into your mission directory as eg tpw_civs.sqf
[5,150,15,2,4,50,0,15,3,1,[],""] 
// [5,150,5,0,0,0,30,[],""]
2 - Call it with 0 = [5,150,5,4,50,0,10,1,[],"spawnscript"] execvm "tpw_civs.sqf"; where 5 = start delay,150 = radius, 5 = how many houses per civilian, 4 = maximum squad inflicted civ casualties, 50 = max total casualties, 0 = what to do if casualty thresholds exceeded (0 - nothing, 1 - popup message, 2 - end mission), 10 = maximum possible civs regardless of density, [] = civilians with these strings in their classnames will be excluded. spawnscript = function or script to run when each civ is spawned. eg "_this call your_function", or "[_this,2] execvm 'your_script.sqf'" where _this is the civ.

THIS SCRIPT WON'T RUN ON DEDICATED SERVERS
*/

if (isDedicated) exitWith {};
if (count _this < 8) exitwith {hint "TPW CIVS incorrect/no config, exiting."};
WaitUntil {!isNull FindDisplay 46};
//WaitUntil {!isnil "tpw_core_sunangle"};

// READ IN VARIABLES
tpw_civ_version = "1.65"; // Version string
tpw_civ_sleep = _this select 0;
tpw_civ_radius = _this select 1;
tpw_civ_density = _this select 2;
tpw_civ_maxsquadcas  = _this select 3;
tpw_civ_maxallcas  = _this select 4;
tpw_civ_casdisplay = _this select 5;
tpw_civ_maxciv = _this select 6;
tpw_civ_init = _this select 7;

// DFAULT VALUES IF MP
/*
if (isMultiplayer) then 
	{
	tpw_civ_sleep = 5;
	tpw_civ_radius = 150;
	tpw_civ_waypoints = 15;
	tpw_civ_density =5;
	};
*/

// VARIABLES
tpw_civ_oldpos = [0,0,0];
tpw_civ_civarray = []; // array holding spawned civs
tpw_civ_houses = []; // array holding civilian houses near player
tpw_civ_civnum = 0; // number of civs to spawn
tpw_civ_debug = false; // Debugging
tpw_civ_allcas = 0; // all civ casualities 
tpw_civ_squadcas = 0; // civ casualities caused by squad
tpw_civ_active = true; // global activate/deactivate
tpw_civ_minstoptime = 6; // minimum time civs will interact
tpw_civ_maxstoptime = 10; // maximum time civs will interact
tpw_civ_resettime = tpw_civ_maxstoptime * 3; // time before civ can interact again
tpw_civ_stoprange = tpw_civ_maxstoptime - tpw_civ_minstoptime; 
tpw_civ_rain = false;
tpw_civ_gunfire = false;
//if (tpw_civ_maxciv > 100) then {tpw_civ_maxciv = 100}; 
tpw_civ_late = false;

tpw_civ_civanims = [
"HubBriefing_think",
"HubBriefing_lookaround1",
"HubBriefing_lookaround2",
"HubBriefing_pointleft",
"HubBriefing_pointright",
"HubBriefing_scratch",
"HubBriefing_stretch",
"Acts_CivilTalking_1",
"Acts_CivilTalking_2",
"Acts_CivilListening_1",
"Acts_CivilListening_2",
"Acts_CivilIdle_1",
"Acts_CivilIdle_2"
];

tov_names_male=["Abel","Ademar","Afonso","Benjamin","Benjamin","Benjamin","Bruno","Bruno","Carles","Cecilio","George","Dario","Luke","Luke","David","David","Edmundo","Elias","Eusebio","Gabino","Hector","Hernan","Ivan","Javier","Leon","Marcio","Ovidio","Paco","Sergio","Teo","Tulio","Xavier"];
tov_names_female=["Ludmila","Ivana","Anna","Petra","Katerina","Vera","Hana","Daniela","Eva","Magda","Bozena","Darja","Natalia","Natasha","Irina","Olga","Zuzana","Marie","Martina","Pavla","Marta","Zlata","Kazi","Libuse","Teta","Linda","Marketa","Eliska","Apolena","Vladimira","Jana","Dagmar","Jaroslava","Nela","Sara","Karolina","Nikola","Gabriela"];
tov_names_last=["Abaroa","Alvarado","Alves","Andres","Blanco","Benitez","Capello","Castillo","De La Cruz","Del Bosque","Esparza","Fernandez","Estevez","Garcia","Gomez","Gutierrez","Hernandez","Jimenez","Jorda","Marquez","Martinez","Moralez","Olmos","Ortega","Pena","Puerta","Ramirez","Robles","Rodriguez","Santiago","Sierra","Torres","Vicario","Villaverde","Zavala"];

// DELAY
sleep tpw_civ_sleep;

// CREATE AI CENTRES SO SPAWNED UNITS KNOW WHO'S AN ENEMY
_centerC = createCenter civilian;

// TEST
tpw_civ_fnc_test = 
	{
	private _unit = _this select 0;
	private _num = _this select 1;
	hint format ["%1 %2",_unit, _num];
	};

// IF CIV IS SHOT BY PLAYER


// SPAWN CIV INTO EMPTY GROUP
tpw_civ_fnc_civspawn =
	{
	private ["_civtype","_civ","_spawnpos","_i","_ct","_sqname","_house","_wp","_wppos"];
	
	// Only bother if no nearby gunfire / explosions
	if (tpw_core_battle) exitwith {};
	
	// Clothing
	tpw_civ_clothes = [] call tpw_core_fnc_clothes;
	
	// Pick a random house for civ to spawn into
	_spawnpos = getpos (tpw_civ_houses select (floor (random (count tpw_civ_houses))));
	//hint str _spawnpos;
	_spawnOk=false;
	while {sleep 0.5;!_spawnOK} do {
		if ( {[getposATL _x, getdir _x, 140, _spawnpos] call bis_fnc_inAngleSector } count allPlayers > 0 ) then {
			_spawnpos = getpos (tpw_civ_houses select (floor (random (count tpw_civ_houses))));
		} else {
			_spawnOk=true;
		};
	};	
	_civtype = tpw_core_civs select (floor (random (count tpw_core_civs)));

	//Spawn civ into empty group
	
	///////////AGENT SECTION//////////////
	_civ=createAgent [_civtype, _spawnpos, [], 0, "NONE"];
	_civ setcaptive true;
	_civ setBehaviour "CARELESS"; 
	_civ disableAI "FSM";
	//_civ setDestination [position player,"LEADER PLANNED",true];
	//_civ forceWalk true;
	_civ enablesimulation false;
	removeallweapons _civ; 
	_civ setAnimSpeedCoef 0.8 + random 0.2;
	_civ setvariable ["tpw_civ_stucktime", diag_ticktime + 60];
	sleep 1.5;
	///////////END OF AGENT SECTION//////////////
	/*
	//////////Not agent section//////////////
		_sqname = creategroup [civilian,true];
		_civ = _sqname createUnit [_civtype,_spawnpos, [], 0, "FORM"]; 
		_civ switchmove "";
		_civ setcaptive true;
		_civ enablesimulation false;
		_civ setskill 0;
		_civ disableAI "TARGET";
		_civ disableAI "FSM";
		_civ disableAI "AUTOTARGET";
		_civ disableAI "AIMINGERROR";
		_civ disableAI "SUPPRESSION"; 
		_civ disableAI "CHECKVISIBLE"; 
		_civ disableAI "COVER"; 
		_civ disableAI "AUTOCOMBAT";
		
		removeallweapons _civ; 
		_civ setAnimSpeedCoef 0.9 + random 0.1;
		 _civ setSpeaker "NoVoice";
		_civ setvariable ["tpw_civ_stucktime", diag_ticktime + 60];
	//////////END OF Not agent section//////////////
	*/
	//Add it to the array of civs for this player
	tpw_civ_civarray set [count tpw_civ_civarray,_civ];
	
	// Random uniform if using BIS civs
	if (["c_man",str _civtype] call BIS_fnc_inString) then
		{
		_civ forceAddUniform (tpw_civ_clothes select  (floor random count tpw_civ_clothes));
		removeheadgear _civ;
		if (random 1 > 0.5) then 
			{
			_civ addheadgear (tpw_core_hats select floor random count tpw_core_hats);
			};
		};
	removegoggles _civ;	
	removevest _civ;
	removebackpack _civ;

	//give them spanish names :
	_first= selectRandom tov_names_male;
	_last=  selectRandom tov_names_last;
	[_civ,[_first+" "+_last,_first,_last]] remoteExec ["setName",0];
	
	/*
	// Mid east civilians
	if (["o_soldier",str _civtype] call BIS_fnc_inString) then
		{
		// Redress soldiers as civs
		_civ forceAddUniform (tpw_civ_clothes select  (floor random count tpw_civ_clothes));
		removeheadgear _civ;
		_civ addheadgear (tpw_core_hats select floor random count tpw_core_hats);
		removebackpack _civ;
		removegoggles _civ;
		removevest _civ;	
		_civ unassignItem "NVGoggles_OPFOR";
		_civ removeItem "NVGoggles_OPFOR";
		removeallweapons _civ; 
		};
	*/
	
	//Mark it as owned by this player
	_civ setvariable ["tpw_civ_owner", [player],true];

	//Add killed/hit eventhandlers


	//Speed and behaviour
		
	// User init	
	if (count tpw_civ_init > 5) then
		{
		_civ call compile tpw_civ_init;
		};	
		
	// Activate civ	
	_civ enablesimulation true;
	};
    
// SEE IF ANY CIVS OWNED BY OTHER PLAYERS ARE WITHIN RANGE, WHICH CAN BE USED INSTEAD OF SPAWNING A NEW CIV
tpw_civ_fnc_nearciv =
	{
	private ["_owner","_shareflag"];
	_shareflag = 0;  

	//Otherwise, spawn a new civ
	if (_shareflag == 0 && {count tpw_civ_houses > 2} && {!tpw_civ_late}) then 
		{
		[] call tpw_civ_fnc_civspawn;    
		};     
	};
	
// PERIODICALLY UPDATE POOL OF ENTERABLE HOUSES NEAR PLAYER, DETERMINE MAX CIVILIAN NUMBER, DISOWN CIVS FROM DEAD PLAYERS IN MP
0 = [] spawn 
	{
	tpw_civ_houses = [tpw_civ_radius] call tpw_core_fnc_screenhouses;
	tpw_civ_civnum = floor ((count tpw_civ_houses) / tpw_civ_density);
	//hint str tpw_core_habhouses;
	while {true} do
		{
		if (tpw_civ_oldpos distance position player > (tpw_civ_radius / 2)) then
			{
			tpw_civ_oldpos = position player;
			private ["_civarray","_deadplayer","_housestring","_uninhab","_house","_i"];
			
			// Scan for habitable houses 
			tpw_civ_houses = [tpw_civ_radius] call tpw_core_fnc_screenhouses;
			tpw_civ_civnum = floor ((count tpw_civ_houses) / tpw_civ_density);			
			if (tpw_civ_civnum > tpw_civ_maxciv) then
					{
					tpw_civ_civnum = tpw_civ_maxciv;
					};
				
			// No new civs if raining, gunfire or very late 
			if (tpw_civ_rain || tpw_civ_late || tpw_core_battle) then 
				{
				tpw_civ_civnum = 0;
				};
					
			};	
		sleep 10.13;
		};
	};

// MAIN LOOP - ADD AND REMOVE CIVS AS NECESSARY
sleep 10;
while {true} do 
	{
	if (tpw_civ_active && speed player < 40) then
		{
		private ["_group","_civ","_i", "_owner","_dests","_dest","_nearcars","_dist"];

		// Too late?
		if (daytime > tpw_core_morning && daytime < tpw_core_night) then
			{
			tpw_civ_late = false;
			} else
			{
			tpw_civ_late = true;
			};
		
		// Rain?
		if (rain > 0.2) then
			{
			tpw_civ_rain = true;
			} else
			{
			tpw_civ_rain = false;
			};	
			
		// Add civs if there are less than the calculated civilian density for the player's current location 
		//(agent _x isKindOf "CAManBase") && (alive agent _x)
		if (( count (agents select {(isAgent _x) && (alive agent _x)}) < tpw_civ_civnum) && (count tpw_civ_houses > 0)) then
			{
				[] spawn tpw_civ_fnc_nearciv;
			};	

		// For each civ...	
		if (count tpw_civ_civarray > 0) then
			{
			for "_i" from 0 to (count tpw_civ_civarray - 1) do
				{
				_civ = tpw_civ_civarray select _i;

				// For live civs
				if (alive _civ) then 
					{
					_dist = _civ distance player;
				
					
					// Is civ still (potentially stuck)?
					if (speed _civ == 0) then
						{
						_civ switchmove "";	
						_civ moveTo position (tpw_civ_houses select floor random count tpw_civ_houses);	// try to  move it to a different house
						_civ forcespeed 10;
						sleep 0.1;
						_civ forcespeed -1;
						} else
						{
						_civ setvariable ["tpw_civ_stucktime", diag_ticktime + 15];
						};	
					
					// Civs run in the rain or in battle or if player has weapon up
					if (tpw_civ_rain || tpw_core_battle ) then 
						{
						_civ forcespeed 10;
						_civ setspeedmode "FULL";
						}
					else
						{
						_civ forcespeed 1;
						_civ setspeedmode "LIMITED";
						};
						
					// Move off the road during battle or rain, or nearby cars
					_nearcars = (_civ nearentities [["car","tank"],8]) select {simulationenabled _x};
					if (tpw_civ_rain || tpw_core_battle || count _nearcars > 0) then
						{
						_civ moveTo (position nearestbuilding _civ);
						};		
						
					// Check if civ is out of range and not visible to player. If so, disown it and remove it from players civ array    
					if ((_dist > tpw_civ_radius || tpw_civ_late || diag_ticktime > _civ getvariable "tpw_civ_stucktime") && ({[getposATL _x, getdir _x, 140, getpos _civ] call bis_fnc_inAngleSector } count allPlayers < 1)) then
						{
						_owner = _civ getvariable "tpw_civ_owner";
						_owner = _owner - [player];
						_civ setvariable ["tpw_civ_owner",_owner,true];            
						tpw_civ_civarray set [_i, -1];       
						};

					// Delete the live civ and its waypoints if it's not owned by anyone    
					if (_civ getvariable "tpw_civ_owner" isequalto []) then
						{
							deletevehicle _civ;						
							sleep 0.5;	
						};    
					} else
					{
					tpw_civ_civarray set [_i, -1]; // removbe civ from player's civ array, but leave dead body   
					};					
				};

			//Update player's civ array    
			tpw_civ_civarray = tpw_civ_civarray - [-1]; 
			player setvariable ["tpw_civarray",tpw_civ_civarray,true];   
			};
		};
	sleep random 5;    
	};  