//execVM "scripts\tpw\startTPW.sqf"


//core, is necessary
//The numbers are the times when civs are awake-> Sahrani is 2nd from last.
//[["C_MAN","C_MAN","C_MAN","C_MAN","C_MAN","C_MAN"],[],5,23] execVM "scripts\tpw\tpw_core.sqf";

//Sounds
//[2,1,1,1,2,0,0,0,5,1] execvm "scripts\tpw\tpw_soap.sqf";
//LOUD : [2,2,2,2,2,0,0,0,5,1] execvm "scripts\tpw\tpw_soap.sqf";

//Civilians
//2 is how many houses per civvie
//[15,150,0.5,0,0,0,30,[],""] execvm "scripts\tpw\tpw_civs.sqf";
//Crowds
//2 is how many civvies per hosue
//[50,2,200,100,50,1.5] execvm "scripts\tpw\tpw_crowd.sqf";
//Parked cars
//[25,300,20,["LOP_TAK_Civ"],1] execvm "scripts\tpw\tpw_park.sqf";

//cars
//[5,3000,5,10,1,[]] execvm "scripts\tpw\tpw_cars.sqf"; 

/*
The following variables can be used to activate/inactivate the various TPW MODS ambient life behaviours whilst the game is running:
- scripts\tpw\tpw_animal_active = true/false
- scripts\tpw\tpw_boat_active = true/false
- scripts\tpw\tpw_car_active = true/false
- scripts\tpw\tpw_civ_active = true/false
- scripts\tpw\tpw_crowd_active = true/false
- scripts\tpw\tpw_duck_active = true/false
- scripts\tpw\tpw_fire_active = true/false
- scripts\tpw\tpw_firefly_active = true/false
- scripts\tpw\tpw_fog_active = true/false
- scripts\tpw\tpw_furniture_active = true/false
- scripts\tpw\tpw_houselights_active = true/false
- scripts\tpw\tpw_park_active = true/false
- scripts\tpw\tpw_radio_active = true/false
- scripts\tpw\tpw_repair_active = true/false
- scripts\tpw\tpw_skirmish_active = true/false
- scripts\tpw\tpw_soap_active = true/false
- scripts\tpw\tpw_streetlights_active = true/false
- scripts\tpw\tpw_zombies_active = true/false
*/