//SP only implementation, kept as backup


canUseFmRadio=true; //--> if false, no radio interaction, and radio don't resume when u get back in a car
isRadioPlaying=false; 

currentFmChannel=0;
currentVolume=0.3;

radioArray=[
//["_radioClass","_radioName","_lenght","_id"]
["","Turn Off",0,0],
["EspantosoFM","Esperantoso Radio",3698,1],
["EastLosFM","East Los FM",2465,2],
["SanJuanRadio","San Juan Radio",3698,3],
["Emotion","Emotion 98.3",3545,4]
];

//[none,esperantoso,eastLFM,SanJuan,Emotion]
currentTimeRadio=[0,0];
{
	currentTimeRadio set [_forEachIndex, random (_x select 2)]; //start Time randomisation
} forEach radioArray ;

//Creating actions, here is the "I'm in a civilian vehicle check" + condition for interation
private _self_fmRadio = ['fmRadio','FM Radio','',{hint format["FM Channel: %1\nVolume: %2%3", (radioArray select currentFmChannel) select 1,currentVolume*100,"%"];},{(canUseFmRadio && (vehicle player) != player) &&  (TypeOf(vehicle player) find "C_" !=-1);}] call ace_interact_menu_fnc_createAction;

private _self_fmRadio_channel = ['fmRadio_channel','Change Channel','',{},{true}] call ace_interact_menu_fnc_createAction;

private _self_fmRadio_volume = ['fmRadio_volume','Change Volume','',{},{true}] call ace_interact_menu_fnc_createAction;

[player, 1, ["ACE_SelfActions"], _self_fmRadio] call ace_interact_menu_fnc_addActionToObject;
[player, 1, ["ACE_SelfActions", "fmRadio"],_self_fmRadio_channel] call ace_interact_menu_fnc_addActionToObject;
[player, 1, ["ACE_SelfActions", "fmRadio"],_self_fmRadio_volume] call ace_interact_menu_fnc_addActionToObject;

//Adding the possible volumes
{
	private _self_volumeValue = [format["volume%1",_x],format["%1%2",_x*100,"%"],'',compile format["currentVolume=%1; 1 fadeMusic %1;", _x],compile format["%1!=currentVolume",_x]] call ace_interact_menu_fnc_createAction;
	[player, 1, ["ACE_SelfActions", "fmRadio","fmRadio_volume"], _self_volumeValue] call ace_interact_menu_fnc_addActionToObject;
} forEach [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1];

//Adding the possible channels
//not MP compatible
{
	_x params ["_radioClass","_radioName","_lenght","_id"];
	private _self_channelValue = [format["channel%1",_radioClass],format["%1",_radioName],'',
	compile format["
		isRadioPlaying=true;
		_oldTimeRadio=getMusicPlayedTime;
		currentTimeRadio set [currentFmChannel,_oldTimeRadio];
		currentFmChannel=%2;
		_startTime=(currentTimeRadio select %2) + random 100;
		if (_startTime > %3) then {_startTime=0;currentTimeRadio set [currentFmChannel,0]};
		playMusic ['%1',_startTime];
		", _radioClass,_id,_lenght],
	compile format["%1!=currentFmChannel",_id]] call ace_interact_menu_fnc_createAction;
	[player, 1, ["ACE_SelfActions", "fmRadio","fmRadio_channel"], _self_channelValue] call ace_interact_menu_fnc_addActionToObject;
} forEach radioArray;

			
// stop music if player leaves vehicle
player addEventHandler ["GetOutMan", {
	params ["_unit", "_role", "_vehicle", "_turret"];
	if (isRadioPlaying) then {
		currentTimeRadio set [currentFmChannel,getMusicPlayedTime];
		playMusic "";
		isRadioPlaying=false;
	}
}];

//restart music if player gets back in 
player addEventHandler ["GetInMan", {
	params ["_unit", "_role", "_vehicle", "_turret"];
	if (canUseFmRadio && (currentFmChannel!=0) &&  (TypeOf(vehicle player) find "C_" !=-1) ) then {
		isRadioPlaying=true;
		_startTime=(currentTimeRadio select currentFmChannel);
		playMusic [(radioArray select currentFmChannel) select 0,_startTime];
	};
}];

//FM Radio loopback
addMusicEventHandler ["MusicStop", {
	params ["_musicClass","_handlerId"];
	if (isRadioPlaying) then {
		playMusic _musicClass;
	};
}];
			