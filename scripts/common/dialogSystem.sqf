/*
Author: Tova inspired by IndeedPete
http://forums.bistudio.com/showthread.php?167159-Group-Callsigns-on-Globalchat-Vehiclechat-and-Commandchat&p=2538835&viewfull=1#post2538835

A chat manager to have people speak to other people.

To use put in init.sqf:
CHAT = compile (preprocessFileLineNumbers "dialogSystem.sqf");

and somewhere else:
[mosby, "Devil-3-3 report, over.", "SIDE"] call CHAT; // script will automatically be delayed so player has time to read
[player, "Devil-3, made contact with unknown scout group, attacked us on sight.", "SIDE"] call CHAT;

*/

//Initialization code
if (isNil "TOV_Chat_Initialized") then {
	TOV_Who_Speaking=objNull;
	TOV_Can_Skip=false;
	player setVariable ["TOV_Skip_Line",false,true];
	player addAction ["Next",{player setVariable ["TOV_Skip_Line",true,true];}, [], 5000, true, false, "", "!(isNull TOV_Who_Speaking) && TOV_Can_Skip"];
	TOV_Chat_Initialized=true;
};

params ["_speaker", "_sentence", "_speakingMode", ["_skippable",false], ["_canMove",true], ["_readingTime",nil], ["_size",1.2]];
_timeToReadOneChar = 0.1;

_speakingMode = toUpper(_speakingMode);
_size=format ["%1", _size];


if (isNil "_readingTime" && !_skippable ) then{
	_readingTime = (count (toArray _sentence) * _timeToReadOneChar);
	if (_readingTime < 2) then {_readingTime = _readingTime+2}; 
} else {
	if (isNil "_readingTime") then {_readingTime=9999;};
};


_lip=true;
_radio=false;
_distanceLimited=false;
_handleMP=false;

waitUntil {isNull TOV_Who_Speaking};
if (_skippable) then { TOV_Can_Skip=true; };

TOV_Who_Speaking=player;
_speaker setVariable ["_isSpeaking",true,true];

_textArray=[];
switch (_speakingMode) do {
	case "SIDE": {_radio=true; _speaker sideChat _sentence};
	case "GROUP": {_speaker groupChat _sentence};
	case "VEH": {_speaker vehicleChat _sentence};
	case "CENTERGROUP": { _textArray= ["<t size='"+_size+"'>"+"<t color='#32B937'>" +format ["%1", name _speaker] +"</t>" +"</t>" + "<br/>"  +_sentence ,0,0.8,1000,0.3]};
	case "CENTERDIRECT": { _distanceLimited=true; _textArray= ["<t size='"+_size+"'>"+ format ["%1", name _speaker] +"</t>" + " <br/>"  +_sentence +"</t>" ,0,0.8,1000,0.3]};
	case "CENTERSIDE": { _radio=true; _textArray= ["<t size='"+_size+"'> "+"<t color='#3A86D1'>" +format ["%1", name _speaker] +"</t>" +"</t>"+ "<br/>"  +_sentence  ,0,0.8,1000,0.3]};
	case "CENTERSIDEVIRTUAL": { _lip=false;_radio=true; _textArray= ["<t size='"+_size+"'>"+"<t color='#3A86D1'>" +format ["%1", _speaker] +"</t>" + "<br/>"  +_sentence +"</t>" ,0,0.8,1000,0.3]};
	case "DESCRIPTION": { _distanceLimited=true;_lip=false; _textArray= ["<br/>"+"<t font='PuristaLight'>"+_sentence +"</t>" ,0,0.8,1000,0.3]};
	case "DIRECT": {_speaker commandChat _sentence};
	case "GLOBAL": {_speaker globalChat _sentence};
	case "DEBUG": {_radio=true; _textArray= ["<t size='"+_size+"'> "+"<t color='#ff0000'>" +format ["%1", name _speaker] +"</t>" +"</t>"+ " <br/>"  +_sentence ,0,0.8,1000,0.3]};
	
	//this one needs special prep to work,
	case "RADIO": {radio setGroupId [format ["%1", name _speaker]]; radio sideChat _sentence; };
	default {hint format ["%1 not recognized!", _speakingMode]};
};

_sound="";
if (_radio) then {
		//WIP
		/*
		_track=2+(floor random 29);
		
		if (_track==9) then {_track=_track+1;};
		_track="rhs_usa_land_rc_"+(str _track);
		playsound _track;
		_sound=ASLToAGL [0,0,0] nearestObject "#soundonvehicle";
		*/
};


_whoWillHear=[player];
if (_distanceLimited) then {
	_whoWillHear=_whoWillHear+ (allPlayers select {_x distance _speaker < 5})
};

_dontMovehandle="";
if (!_canMove) then {
	_dontMovehandle=(findDisplay 46) displayAddEventHandler ["KeyDown", {
		params ["_displayorcontrol", "_key", "_shift", "_ctrl", "_alt"];
		_disabledKeys=[];
		{
			_disabledKeys=_disabledKeys+actionKeys _x
		} forEach ["MoveForward","MoveBack","TurnLeft","TurnRight","MoveFastForward","MoveSlowForward","EvasiveLeft","EvasiveRight"];
		
		if (_key in _disabledKeys) then {true};
	}];	
};

[[_textArray,TOV_Who_Speaking,_speaker,_distanceLimited,_lip,_readingTime],{
	params ["_textArray","_conversationInstigator","_speaker","_distanceLimited","_lip","_readingTime"];
	
	if ((isNull TOV_Who_Speaking) || (TOV_Who_Speaking==_conversationInstigator)) then {
		//hint Format ["%1",_readingTime];
		_textArray spawn TOV_fnc_dynamicText;
	
		_startTime=time;
		if (_lip) then {
			_speaker setRandomLip true;
		};
		
		//Edit below to handle case : remote listening to skipable conversation.
		waitUntil{(time> (_startTime+_readingTime)) || (_conversationInstigator getVariable "TOV_Skip_Line") || ((_speaker distance player>5) && _distanceLimited)};
		_speaker setRandomLip false;
		if (player==_conversationInstigator) then {
			player setVariable ["TOV_Skip_Line",false,true];
			TOV_Can_Skip=false;
			TOV_Who_Speaking=objNull;
		};
		
		if ((isNull TOV_Who_Speaking) || (TOV_Who_Speaking==_conversationInstigator)) then {
			//hint format["Why I stopped : %1",_textArray ];
			//_clear=["",0,0,1,0.1] spawn TOV_fnc_dynamicText;
			_clear=["",0,0,10,0.1] spawn TOV_fnc_dynamicText;
			//if (_radio) then { deleteVehicle _sound;};
		};
	};
	
}] remoteExec ["bis_fnc_call", _whoWillHear]; 

waitUntil {isNull TOV_Who_Speaking};
if (!_canMove) then {
	(findDisplay 46) displayRemoveEventHandler ["keyDown",_dontMovehandle];
};
_speaker setVariable ["_isSpeaking",false,true];
