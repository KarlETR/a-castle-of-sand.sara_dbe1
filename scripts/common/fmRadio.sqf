//MP compatible implementation

canUseFmRadio=true; //--> if false, no radio interaction, and radio don't resume when u get back in a car
isRadioPlaying=false; 

player setVariable ["currentFmChannel",0,true];
player setVariable ["currentVolume",0.3,true];

radioArray=[
	//["_radioClass","_radioName","_lenght","_id"]
	["","Turn Off",0,0],
	["EspantosoFM","Esperantoso Radio",3698,1],
	["EastLosFM","East Los FM",2465,2],
	["SanJuanRadio","San Juan Radio",3698,3],
	["Emotion","Emotion 98.3",3545,4]
];

//debug lines
private _dummyFmRadio="TOV_dummyFmRadio" call BIS_fnc_getParamValue;
if (_dummyFmRadio==1) then {
	{
		radioArray #_forEachIndex set [2,_x];
	} forEach [0,270,210,186,358];
};
	

//[none,esperantoso,eastLFM,SanJuan,Emotion]
currentTimeRadio=[0,0];
{
	currentTimeRadio set [_forEachIndex, random (_x#2)]; //start Time randomisation
} forEach radioArray ;
player setVariable ["currentTimeRadio",currentTimeRadio,true];

//values Updater:
updatePlaybackData={
	while {sleep 0.2;isRadioPlaying} do {
		_locCurrentTimeRadio= player getVariable 'currentTimeRadio';
		_locCurrentFmChannel = player getVariable 'currentFmChannel';
		_locCurrentTimeRadio set [_locCurrentFmChannel,getMusicPlayedTime];
		player setVariable ['currentTimeRadio',_locCurrentTimeRadio,true];
	};
};

//Creating actions, here is the "I'm in a civilian vehicle check" + condition for interation
private _self_fmRadio = ['fmRadio','FM Radio','',{
		hint format["FM Channel: %1\nVolume: %2%3",
		(radioArray select (player getVariable 'currentFmChannel')) select 1,
		(player getVariable 'currentVolume')*100,
		"%"];
	},{
		_vehiclePlayer=getText (configfile >> "CfgVehicles" >> typeOf (vehicle player)  >> "faction");
		(canUseFmRadio && (vehicle player) != player) && ((_vehiclePlayer find "_C_" !=-1) || (_vehiclePlayer find "CIV" !=-1));
	}] call ace_interact_menu_fnc_createAction;

private _self_fmRadio_channel = ['fmRadio_channel','Change Channel','',{},{true}] call ace_interact_menu_fnc_createAction;

private _self_fmRadio_volume = ['fmRadio_volume','Change Volume','',{},{true}] call ace_interact_menu_fnc_createAction;

[player, 1, ["ACE_SelfActions"], _self_fmRadio] call ace_interact_menu_fnc_addActionToObject;
[player, 1, ["ACE_SelfActions", "fmRadio"],_self_fmRadio_channel] call ace_interact_menu_fnc_addActionToObject;
[player, 1, ["ACE_SelfActions", "fmRadio"],_self_fmRadio_volume] call ace_interact_menu_fnc_addActionToObject;

//Adding the possible volumes
{
	private _self_volumeValue = [format["volume%1",_x],format["%1%2",_x*100,"%"],'',
		compile format["
				[
					[vehicle player,%1],
					{
						params ['_car','_newVolume'];
						if (player in _car) then { 
							player setVariable ['currentVolume',_newVolume,true];
							1 fadeMusic _newVolume;
						};
					}
				] remoteExecCall ['bis_fnc_call', 0]; 
			", _x],
		compile format["%1!=(player getVariable 'currentVolume')",_x]] call ace_interact_menu_fnc_createAction;
		
	[player, 1, ["ACE_SelfActions", "fmRadio","fmRadio_volume"], _self_volumeValue] call ace_interact_menu_fnc_addActionToObject;
} forEach [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1];

//Adding the possible channels
{
	_x params ["_radioClass","_radioName","_lenght","_id"];
	private _self_channelValue = [format["channel%1",_radioClass],format["%1",_radioName],'',
	compile format["
		[%4%1%4,%2,%3] params ['_radioClass','_id','_lenght'];
		_oldTimeRadio=getMusicPlayedTime;
		_randomSkip=random 100;
		[
			[_radioClass,_id,_lenght,_oldTimeRadio,vehicle player,_randomSkip],
			{
				params ['_radioClass','_id','_lenght','_oldTimeRadio','_car','_randomSkip'];
				if (player in _car) then {
					isRadioPlaying=true;
					_locCurrentTimeRadio= player getVariable 'currentTimeRadio';
					_locCurrentFmChannel = player getVariable 'currentFmChannel';
					_locCurrentTimeRadio set [_locCurrentFmChannel,_oldTimeRadio];
					player setVariable ['currentTimeRadio',_locCurrentTimeRadio,true];
					player setVariable ['currentFmChannel',_id,true];
					_startTime=(_locCurrentTimeRadio select _id) + _randomSkip;
					if (_startTime > _lenght) then {
						_startTime=0;
						_locCurrentTimeRadio set [_locCurrentFmChannel,0];
						player setVariable ['currentTimeRadio',_locCurrentTimeRadio,true];
					};
					playMusic [_radioClass,_startTime];
					[] spawn updatePlaybackData;

				};
			}
		] remoteExecCall ['bis_fnc_call', 0]; 
		", _radioClass,_id,_lenght,"'"],
	compile format["%1!=player getVariable 'currentFmChannel'",_id]] call ace_interact_menu_fnc_createAction;
	[player, 1, ["ACE_SelfActions", "fmRadio","fmRadio_channel"], _self_channelValue] call ace_interact_menu_fnc_addActionToObject;
} forEach radioArray;

			
// stop music if player leaves vehicle
player addEventHandler ["GetOutMan", {
	params ["_unit", "_role", "_vehicle", "_turret"];
	if (isRadioPlaying) then {
		_locCurrentTimeRadio= player getVariable 'currentTimeRadio';
		_locCurrentFmChannel = player getVariable 'currentFmChannel';
		_locCurrentTimeRadio set [_locCurrentFmChannel,getMusicPlayedTime];
		player setVariable ["currentTimeRadio",_locCurrentTimeRadio,true];
		playMusic "";
		isRadioPlaying=false;
	}
}];



//restart music if player gets back in 
player addEventHandler ["GetInMan", {
	params ["_unit", "_role", "_vehicle", "_turret"];
	
	_shouldPlayRadio={
		_vehiclePlayer=getText (configfile >> "CfgVehicles" >> typeOf (vehicle player)  >> "faction");
		(canUseFmRadio && (vehicle player) != player) && ((_vehiclePlayer find "_C_" !=-1) || (_vehiclePlayer find "CIV" !=-1));
	};
	
	if (canUseFmRadio && (call _shouldPlayRadio) ) then {
		_playersInCar=((fullCrew _vehicle) apply {_x select 0} select {isPlayer _x}) - [player];
		_reference=player;
		if (count _playersInCar>0) then {
			_reference=_playersInCar select 0;
		};	

		isRadioPlaying=true;
		_startTime=((_reference getVariable 'currentTimeRadio') select (_reference getVariable 'currentFmChannel'));
		playMusic [(radioArray select (_reference getVariable 'currentFmChannel')) select 0,_startTime];
		_newVolume=_reference getVariable 'currentVolume';
		0 fadeMusic _newVolume;
		player setVariable ["currentFmChannel",_reference getVariable 'currentFmChannel',true];
		player setVariable ["currentVolume",_newVolume,true];
		[] spawn updatePlaybackData;
	};
}];



//FM Radio loopback
addMusicEventHandler ["MusicStop", {
	params ["_musicClass","_handlerId"];
	if (isRadioPlaying) then {
		playMusic _musicClass;
	};
}];
			