//Ace view distance
currentViewDistance = viewDistance;
private _self_viewdistance = ['ViewDistance','Set view distance','',{hint format["Current view distance: %1", currentViewDistance];},{true}] call ace_interact_menu_fnc_createAction;

[player, 1, ["ACE_SelfActions"], _self_viewdistance] call ace_interact_menu_fnc_addActionToObject;

{
	private _self_viewvalue = [format["view%1",_x],format["%1",_x],'',compile format["currentViewDistance=%1;setViewDistance %1;", _x],compile format["%1!=currentViewDistance",_x]] call ace_interact_menu_fnc_createAction;
	[player, 1, ["ACE_SelfActions", "ViewDistance"], _self_viewvalue] call ace_interact_menu_fnc_addActionToObject;
} forEach [500,1000,1500,2000,2500,3000,3500,4000,5000,6000,7000,8000,9000,10000,12000];