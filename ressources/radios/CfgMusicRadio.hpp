	class EspantosoFM
	{
		// display name
		name	= "Espantoso FM";

		// filename, volume, pitch
		sound[]	= { "\ressources\radios\EspantosoFM.ogg", db + 0, 1.0 };
	};

	class EastLosFM
	{
		// display name
		name	= "East Los FM";
		// Lenght = 2465 
		// filename, volume, pitch
		sound[]	= { "\ressources\radios\EastLosFM.ogg", db + 16, 1.0 };
	};
	
	class SanJuanRadio
	{
		// display name
		name	= "San Juan Radio";
		// Lenght = 3698 
		// filename, volume, pitch
		sound[]	= { "\ressources\radios\SanJuanRadio.ogg", db + 16, 1.0 };
	};
		
	class Emotion
	{
		// display name
		name	= "Emotion 98.3";
		// Lenght = 3545  
		// filename, volume, pitch
		sound[]	= { "\ressources\radios\Emotion.ogg", db + 0, 1.0 };
	};
		
	